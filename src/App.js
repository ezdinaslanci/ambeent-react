import logo from './logo.svg';
import './App.css';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import React, { useEffect, useState } from "react";
import { Button } from '@material-ui/core';
import DataSet from './data.json';

function getData() {
  return DataSet;
}
let data = getData();



const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'left',
  },
  paperCtesi: {
    padding: theme.spacing(2),
    textAlign: 'left',
    background:'#fffaf0',
  },
  paperPazar: {
    padding: theme.spacing(2),
    textAlign: 'left',
    background:'#faefe6',
  },
  textField: {
    margin: '0 10px',
    top: '-5px',
  }
}));
function toTime(hour, minute){
  return ((hour < 9) ? ("0" + hour) : hour) + ":" +  ((minute < 9) ? ("0" + minute) : minute);  
}
function App() {
  setTimeout(2000);
  const classes = useStyles();
  const [state, setState] = React.useState({
    defaultStartTime: toTime(data.default.startHour, data.default.startMinute)
  });
  const handleDefaultStartTimeChange = (event) => {
    state.defaultStartTime = event.target.value;
    if(event.target.value  != toTime(data.default.startHour, data.default.startMinute)){
      setState({ ...state, ["saveDefault"]: true });
    }
    else{
      setState({ ...state, ["saveDefault"]: false });
    }
  };
  const saveDefaultClicked = (event) => {
    let splittedTime = state.defaultStartTime.split(":");
    data.default.startHour = Number(splittedTime[0]);
    data.default.startMinute = Number(splittedTime[1]);
    setState({ ...state, ["saveDefault"]: false });
  };
  return (
    <div className={classes.root}>
      <h5>Ayarlar / Çalışma Saatleri</h5> 
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className={classes.paper}><label>Yetkili Servis Çalışma Saatlerini buradan ayarlayabilirsiniz</label></Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <h3>Varsayılan Çalışma Saati</h3>
            <label>Bu ayar tatil olan günlerde, gece nöbetinin ayarlanabilmesi için önemlidir.</label>
            </Paper>
            <Paper className={classes.paper}> 
            <p>Başlangıç : 
            <TextField
              id="time"
              label=""
              type="time"
              name="defaultStartTime"
              defaultValue={state.defaultStartTime}
              className={classes.textField}
              onChange={handleDefaultStartTimeChange}
              InputLabelProps={{
                shrink: true,
              }}
              inputProps={{
                step: 300, // 5 min
              }}
            />
            <Button name='saveDefault' disabled={!state.saveDefault} onClick={saveDefaultClicked}>KAYDET</Button></p>
          </Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={classes.paper}>
            <p>Gün</p>
            <p><b>Pazartesi</b></p>
            <DayItem dayNumber = {0}/>
            </Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={classes.paper}>
            <p>Gün</p>
            <p><b>Salı</b></p>
            <DayItem dayNumber = {1}/>
            </Paper>
        </Grid>
        <Grid item xs={3}>
            <Paper className={classes.paper}>
            <p>Gün</p>
            <p><b>Çarşamba</b></p>
            <DayItem dayNumber = {2}/>
            </Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={classes.paper}>
            <p>Gün</p>
            <p><b>Perşembe</b></p>
            <DayItem dayNumber = {3}/>
            </Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={classes.paper}>
            <p>Gün</p>
            <p><b>Cuma</b></p>
            <DayItem dayNumber = {4}/>
            </Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={classes.paperCtesi}>
            <p>Gün</p>
            <p><b>Cumartesi</b></p>
            <DayItem dayNumber = {5}/>
            </Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={classes.paperPazar}>
            <p>Gün</p>
            <p><b>Pazar</b></p>
            <DayItem dayNumber = {6}/>
            </Paper>
        </Grid>
      </Grid>
    </div>
  );
}


const DayItem = ({ dayNumber }) =>{
  
  const checkDisableButton = () => {
    let isHolidayChanged = state.isHoliday != data.shift[dayNumber].isHoliday;
    let startTimeChanged = state.startTime != toTime(data.shift[dayNumber].startHour, data.shift[dayNumber].startMinute);
    let finishTimeChanged = state.finishTime != toTime(data.shift[dayNumber].finishHour, data.shift[dayNumber].finishMinute);
    if(isHolidayChanged || startTimeChanged || finishTimeChanged){
      setState({ ...state, ["saveB"]: true });
    }
    else{
      setState({ ...state, ["saveB"]: false });
    }
  };

  const [state, setState] = React.useState({
    startTime: toTime(data.shift[dayNumber].startHour, data.shift[dayNumber].startMinute),
    finishTime: toTime(data.shift[dayNumber].finishHour, data.shift[dayNumber].finishMinute),
    isHoliday: data.shift[dayNumber].isHoliday
  });
  const handleChange = (event) => {
    state.isHoliday = event.target.checked;
    setState({ ...state, [event.target.name]: event.target.checked });
    checkDisableButton();
  };
  const handleStartTimeChange = (event) => {
    state.startTime = event.target.value;
    checkDisableButton();
    
  };
  const handleEndTimeChange = (event) => {
    state.finishTime = event.target.value;
    checkDisableButton();
  };
  const saveDayClicked = (event) => {
    let splittedStartTime = state.startTime.split(":");
    data.shift[dayNumber].startHour = Number(splittedStartTime[0]);
    data.shift[dayNumber].startMinute = Number(splittedStartTime[1]);

    let splittedFinishTime = state.finishTime.split(":");
    data.shift[dayNumber].finishHour = Number(splittedFinishTime[0]);
    data.shift[dayNumber].finishMinute = Number(splittedFinishTime[1]);

    data.shift[dayNumber].isHoliday = state.isHoliday;
    setState({ ...state, ["saveB"]: false });
  };
  const classes = useStyles();
  return (
    <div>
      <Switch
        checked={state.isHoliday}
        onChange={handleChange}
        color="primary"
        name="isHoliday"
        inputProps={{ 'aria-label': 'primary checkbox' }}
      /> Tatil

      <p>Başlangıç : 
      <TextField
        id="time"
        label=""
        type="time"
        defaultValue={state.startTime}
        className={classes.textField}
        onChange={handleStartTimeChange}
        InputLabelProps={{
          shrink: true,
        }}
        inputProps={{
          step: 300, // 5 min
        }}
      /></p>
      <p>Bitiş : 
      <TextField
        id="time"
        label=""
        type="time"
        defaultValue={state.finishTime}
        className={classes.textField}
        onChange={handleEndTimeChange}
        InputLabelProps={{
          shrink: true,
        }}
        inputProps={{
          step: 300, // 5 min
        }}
      /></p>
      <Button name="saveB" disabled={!state.saveB} onClick={saveDayClicked} className="dayButton">KAYDET</Button>
    </div>
  );
}

export default App;
